package com.kevin.training.Practice;

public class Task {
	
	private String description;
	private int duration;
	
	public Task(){
		
	}
	public Task(String description, int duration){
	
	this.description=description;
	this.duration=duration;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

}
